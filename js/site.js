$(function () {

    $('.slider_testimonials').slick({
        arrows: false,
        asNavFor: '.carousel_testimonials',
        fade: true
    });

    $('.carousel_testimonials').slick({
        slidesToShow: 4,
        asNavFor: '.slider_testimonials',
        focusOnSelect: true
    });

    $('.gallery__grid').masonry({
        itemSelector: '.gallery__item',
        percentPosition: true,
        columnWidth: '.gallery__grid-sizer',
        gutter: 18
    });

    $('#appendGallery').on('click', function () {

        const btnContent = $(this).html();
        $(this).html('<div class="lds-ring"><div></div><div></div><div></div><div></div></div>');

        let serverDelay = setTimeout( () => {

            function generateTemplate(){
                const template = `
                <div class="gallery__item">
                    <div class="gallery__controls">
                        <a href="#" class="gallery__control-btn"><i class="icon-search"></i></a>
                        <a href="#" class="gallery__control-btn"><i class="icon-expand"></i></a>
                    </div>
                    <img class="gallery__img-item" src="img/gallery/lg_0${Math.floor(Math.random() * 6 + 1)}.jpg" alt="image">
                </div>`;
                return template;
            }

            let $item = $(generateTemplate() + generateTemplate() + generateTemplate());

            $('.gallery__grid').append($item).masonry('appended', $item);

            $(this).html(btnContent);
            clearInterval(serverDelay);

        },2000);

    });

    const siteJS = {
        init() {
            this.tabs('.tabs');
            this.checkYOffset('.header', 300);
            $(window).on('scroll', () => this.checkYOffset('.header', 300));
        },
        checkYOffset(element, maxYOffset) {
            window.pageYOffset > maxYOffset?
                $(element).addClass('maxYOffset'):
                $(element).removeClass('maxYOffset');
        },
        tabs(tabsWrap) {

            $(tabsWrap).each(function () {

                const controlPad = $(this).find('.tabs__head');
                const contentWrapper = $(this).find('.tabs__body');
                const uploadBtn = $(this).find('.tabs__upload .btn');
                const UPLOADQUANTITY = 12;

                controlPad.on('click', function (event) {

                    if($(event.target).hasClass('tabs__anchor')){

                        const controlItems = $(this).find('.tabs__anchor');
                        const currentTab = $(event.target).data('tabsAnchor');
                        const hiddenEls = getHiddenEls(contentWrapper, currentTab);

                        controlItems
                            .not($(event.target))
                            .removeClass('active');

                        $(event.target).addClass('active');

                        toggleTabsContent(contentWrapper, currentTab);

                        if(uploadBtn.length && hiddenEls.length){
                            uploadBtn.show();
                        } else if(!uploadBtn.length || uploadBtn.length && !hiddenEls.length){
                            uploadBtn.hide();
                        }

                    }
                });

                if(uploadBtn.length){
                    uploadBtn.on('click', function () {

                        const btnContent = $(this).html();
                        $(this).html('<div class="lds-ring"><div></div><div></div><div></div><div></div></div>');

                        let serverDelay = setTimeout( () => {

                            const currentTab = controlPad.find('.active').data('tabsAnchor');
                            const hiddenEls = getHiddenEls(contentWrapper, currentTab);

                            if(hiddenEls.length){
                                for(let i = 0; i < UPLOADQUANTITY; i++){
                                    $(hiddenEls)
                                        .eq(i)
                                        .removeClass('fakeUpload')
                                        .addClass('active');
                                }
                            }

                            if(!getHiddenEls(contentWrapper, currentTab).length){
                                $(this).hide();
                            }

                            $(this).html(btnContent);
                            clearInterval(serverDelay);

                        },2000);

                    })
                }

            });

            function getHiddenEls(contentWrap, activeTab){
                const hiddenEls = contentWrap.find('.fakeUpload');

                return jQuery.grep(hiddenEls, function (item) {
                    return $(item).data('tabsClass') === activeTab || activeTab === 'all';
                });
            }

            function toggleTabsContent(contentWrap, anchor) {

                const contentItems = contentWrap.find('.tabs__body-item');

                if(anchor === 'all'){
                    contentItems.each(function () {
                        if(!$(this).hasClass('fakeUpload')){
                            $(this).addClass('active');
                        }
                    });
                    return
                }

                contentItems.each(function () {
                    if($(this).data('tabsClass') === anchor && !$(this).hasClass('fakeUpload')){
                        $(this).addClass('active')
                    } else {
                        $(this).removeClass('active')
                    }
                })
            }
        }
    };

    siteJS.init()

});